#include <iostream>
#include <string>
#include <vector>

/// Строчка кода ниже показывает, что мы используем пространство имен std.
/// Нам не нужно писать std::cout, а можно cout и т. д.
using namespace std;

void print(int num) {
    cout << num << endl;
}

int main()
{
    ///типы данных
    int a = 0;
    a = 1 + a;

    double b = double(0);
    char c = 'g';
    string s1 = "sdasdsd";
    string s2 = "abc";
    bool d = true;

    int number = 0;

    ///пример с ++
    print(number);
    print(++number);
    print(number++);
    print(number);

    ///векторы

    vector<int> v1;

    vector<int> v2(10,5); //вектор длиной 10, состоящий из цифр "5"

    vector<int> v3 = {6,2,3,4,5}; //вектор [1,2,3,4,5]

    ///циклы

    for(int i = 0; i < v3.size(); ++i) {
        print(v3[i]);
    }

    int j = 0;
    while(j < v3.size()) {
        print(v3[j]);
        j += 2; //j = j+2;
    }


}
