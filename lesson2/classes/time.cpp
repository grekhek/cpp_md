#include "time.h"

#include <iostream>

using namespace std;

/// Конструктор по умолчанию
Time::Time()
/// Далее идет список инициализации полей класса. Здесь мы можем задать значения даже для константных полей.
/// Здесь нету присваивания, а его нельзя делать для констант
    : _name("timer")
    , _seconds(0)
{

}

/// Конструктор с параметрами
Time::Time(const std::string name, int seconds)
    : _name(name)
    , _seconds(seconds)
{

}

Time::~Time()
{

}

string Time::GetName() const
{
    return _name;
}

int Time::GetSeconds() const
{
    return _seconds;
}

void Time::SetSeconds(int seconds)
{
    _seconds = seconds;
}

/// Оператор =
Time &Time::operator=(const Time &t)
{
    /// this - указатель на текущий объект класса
    /// так как это указатель то для обращения к методам требуется использовать ->
    /// тут мы присваиваем новое значение секунд текущему объекту класса
    this->_seconds = t.GetSeconds();
    /// возвращаем разыменованый указатель на текущий объект класса
    return *this;
}

/// Оператор +
Time operator+(const Time &lhs, const Time &rhs)
{
    /// Возвращаем новый объект класса(вызываем конструктор)
    /// У нового объекта будет значение имени такое как у левого значения
    /// Значения секунд складываются и отправляются в конструктор
    return Time(lhs.GetName(), lhs.GetSeconds() + rhs.GetSeconds());
}

/// Оператор вывода в поток в формате "Имя : 0h 0m 0s"
ostream &operator<<(ostream &os, const Time &t)
{
    int seconds, minutes, hours;

    /// Простая математика ^_^
    seconds = t.GetSeconds() % 60;
    minutes = t.GetSeconds() / 60;
    hours = minutes / 60;
    minutes %= 60;
    /// Выводим в поток требуемые значения
    os << t.GetName() << " : " << hours << "h " << minutes << "m " << seconds << "s";
    /// Возвращаем поток
    return os;
}

///Оператор меньше(для сортировки нужен)
bool operator<(const Time &lhs, const Time &rhs)
{
    return lhs.GetSeconds() < rhs.GetSeconds();
}
