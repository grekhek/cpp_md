#pragma once
#include <string>
#include <iostream>

class Time
{
    const std::string _name;
    int _seconds;
public:
    ///Конструкторы
    Time();
    Time(const std::string name, int seconds);

    ///Деструктор
    ~Time();

    ///Методы
    std::string GetName() const; // Это константный метод, т е тот который не позволяет менять поля класса. Ключевое слово const после метода
    int GetSeconds() const;
    void SetSeconds(int seconds);

    /// Перегружаем оператор =
    Time& operator=(const Time& t);
};

/// Перегружаем операторы +, < и <<
Time operator+(const Time& lhs, const Time& rhs);
bool operator<(const Time& lhs, const Time& rhs);
std::ostream& operator<<(std::ostream& os, const Time& t);


